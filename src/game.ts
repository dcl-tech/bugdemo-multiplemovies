import {spawnBoxX, spawnTextX, spawnEntity} from 'modules/SpawnerFunctions'

export type FilmInfo = {
  title:string
  duration:number //seconds
  description:string
  resource:string // filepath or https URL w/ CORS
  contexts:string[]
}

let defaultCatalog:FilmInfo[] = 
[
  {
      title: "The Dragon's Dream",
      duration: 178,
      description: "What a dragon dreamed once upon a time",
      resource: "https://extraterra-files.nyc3.digitaloceanspaces.com/video/CarlFravel/TheDragonsDream/TheDragonsDream.m3u8",
      contexts: ["cc-cinema"]
  }
  ,
  {
      title: "The Park",
      duration: 62,
      description: "Video Painted Portrait of a Young Boy",
      resource: "https://extraterra-files.nyc3.digitaloceanspaces.com/video/CarlFravel/The_Park_intro/The_Park_intro.m3u8",
      contexts: ["cc-cinema"]
  }
  ,
  {
      title: "Crypto Plus Virtual Worlds",
      duration: 390,
      description: "A talk given Sept 2020 by Carl Fravel for the Crypto College launch of the Decentraland University",
      resource: "https://extraterra-files.nyc3.digitaloceanspaces.com/video/CarlFravel/DCLU_Crypto_College_talk_Carl_Fravel/DCLU_Crypto_College_talk_Carl_Fravel.m3u8",
      contexts: ["cc-cinema"]
  }
  
]

///////////////////////////////////////////////////////////
/////// CinemaControls
///////////////////////////////////////////////////////////

class CinemaControls {
  catalogSelectionIndex:number = 0

  cinemaClient:CinemaClient

  buttonBase:Entity

  previousButton:Entity
  nextButton:Entity
  playButton:Entity
  pauseButton:Entity

  catalogDisplay:Entity
  
  constructor (cinemaClient:CinemaClient) {
      this.cinemaClient = cinemaClient
      this.setupUI()
  }

  setupUI (){

    let st = this.cinemaClient.screen.getComponent(Transform)

    this.buttonBase = spawnEntity(st.position.x, 0, st.position.z, 0,0,0, 1,1,1)
    
    // Prev
    this.previousButton = spawnBoxX(-.75,1,-9, 0,0,0, 0.3, 0.3, 0.3)
    this.previousButton.setParent(this.buttonBase)
    this.previousButton.addComponent(new OnClick(() => {
        if (this.catalogSelectionIndex > 0) {
            this.catalogSelectionIndex--
            this.displayCatalog()
        }
    }))

    // PLAY 
    this.playButton = spawnBoxX(-0.25,1,-9, 0,0,0, 0.3, 0.3, 0.3)
    this.playButton.setParent(this.buttonBase)
    this.playButton.addComponent(new OnClick(() => {
        if (this.cinemaClient.currentlyPlayingIndex != this.catalogSelectionIndex) {
            if (this.cinemaClient.state == this.cinemaClient.PLAYING || this.cinemaClient.state == this.cinemaClient.PAUSED) {
                this.cinemaClient.stopVideo()
            }
            //this.cinemaClient.currentlyPlayingIndex = this.catalogSelectionIndex
        }
        if (this.cinemaClient.state != this.cinemaClient.PLAYING) {
            if (this.cinemaClient.state != this.cinemaClient.PAUSED) {
                this.cinemaClient.loadVideo(this.catalogSelectionIndex)
            }
            this.cinemaClient.playVideo()
        }
        this.displayCatalog()
    }))

    // PAUSE
    this.pauseButton = spawnBoxX(0.25,1,-9, 0,0,0, 0.3, 0.3, 0.3)
    this.pauseButton.setParent(this.buttonBase)
    this.pauseButton.addComponent(new OnClick(() => {
        if (this.cinemaClient.state == this.cinemaClient.PLAYING) {
            this.cinemaClient.pauseVideo()
        }
        this.displayCatalog()
    }))

    // Next
    this.nextButton = spawnBoxX(.75,1,-9, 0,0,0, 0.3, 0.3, 0.3)
    this.nextButton.setParent(this.buttonBase)
    this.nextButton.addComponent(new OnClick(() => {
        if (this.catalogSelectionIndex < this.cinemaClient.catalog.length-1) {
            this.catalogSelectionIndex++
            this.displayCatalog()
        }
    }))

    // Catalog Display
    this.catalogDisplay = spawnTextX("", -3,1,-9, 0,0,0, .2,.2,.2)
    this.catalogDisplay.setParent(this.buttonBase)

    // Display the current movie
    this.displayCatalog()
  }

  displayCatalog(){
    // Display the info about the currently selected movie:
    this.catalogDisplay.getComponent(TextShape).value = this.cinemaClient.catalog[this.catalogSelectionIndex].title + "\n\n" 
    + this.cinemaClient.catalog[this.catalogSelectionIndex].description + "\n\n"
    + Math.floor(this.cinemaClient.catalog[this.catalogSelectionIndex].duration/60+1)  + " min"
    + (this.cinemaClient.currentlyPlayingIndex == this.catalogSelectionIndex?(this.cinemaClient.state==this.cinemaClient.PLAYING?"  Playing":"  Paused"):"")

    if (this.cinemaClient.currentlyPlayingIndex == this.catalogSelectionIndex && (this.cinemaClient.state==this.cinemaClient.PLAYING||this.cinemaClient.state==this.cinemaClient.PAUSED)) {
        this.catalogDisplay.getComponent(TextShape).color = Color3.Green()
    }
    else {
        this.catalogDisplay.getComponent(TextShape).color = Color3.Gray()
    }
  }

  maximizeUI(){
      this.displayCatalog()
  }

  selectPrevious(){
      if (this.catalogSelectionIndex>0) {
          this.catalogSelectionIndex--
          this.displayCatalog()
      }
  }
  selectNext(){
      if (this.catalogSelectionIndex<this.cinemaClient.catalog.length-1) {
        this.catalogSelectionIndex++
        this.displayCatalog()
      }
  }
}


///////////////////////////////////////////////////////////
/////// CinemaClient
///////////////////////////////////////////////////////////


export class CinemaClient implements ISystem{
  admit:number
  mode:string
  venueID:string
  catalogID:string
  catalogContext:string
  catalog:FilmInfo[]
  currentlyPlayingIndex:number
  screen:Entity
  cinemaControls:CinemaControls
  videoTexture:VideoTexture
  videoMaterial:Material
  state:number

  INIT:number = 0
  LOADED:number = 1
  PLAYING:number = 2
  PAUSED:number = 3

  constructor (venueID:string, catalogID:string, screen:Entity, catalog:FilmInfo[]) {
    this.venueID = venueID
    this.catalogID = catalogID
    this.catalog = catalog
    this.screen = screen
    this.state = this.INIT
    engine.addSystem(this)

    this.mode = "JUKEBOX" // TODO get it from server for this context
    this.admit = -1 // TODO get it from server for this user's Eth Address

    // before calling the serer to look up a catalog, make sure that params are "", not null
    if (this.venueID == null) this.venueID = ""
    if (this.catalogID == null) this.catalogID = ""
    this.catalog = null // TODO replace this with server lookup of catalog(venueID,catalogID).
    // TODO handle this lookup being asynchronous
     if (this.catalog == null || this.catalog.length == 0) {
        // no passed-in catalog, so use the default one
        // TODO even look THIS up from server
        // TODO decide whether we actually still look that up from server (venueID="",catalogID="")
        this.catalog = defaultCatalog
    }
    this.currentlyPlayingIndex = 0 // TODO look it up from server(venue,context)

    this.cinemaControls = new CinemaControls(this) 

  }
  loadVideo(catalogIndex) {
    log("LOAD")
    // if (this.state == this.INIT || (catalogIndex != this.currentlyPlayingIndex && (this.state == this.PAUSED || this.state == this.PLAYING))) {
        if (this.videoTexture != null && this.videoTexture.playing == true) {
            this.videoTexture.reset()
        }
        this.currentlyPlayingIndex = catalogIndex

        let audioStream:AudioStream = this.screen.getComponentOrNull(AudioStream)
        if (audioStream != null) {
            log("AudioStream not null")
            this.screen.removeComponent(audioStream)
        }
        let audioclip:AudioClip = this.screen.getComponentOrNull(AudioClip)
        if (audioclip != null) {
            log("Audioclip not null")
            this.screen.removeComponent(audioclip)
        }

        this.videoTexture = new VideoTexture(new VideoClip(this.catalog[this.currentlyPlayingIndex].resource))
        this.videoMaterial = new Material()
        this.videoMaterial.albedoTexture = this.videoTexture
        this.videoMaterial.roughness = 1.0
        this.screen.addComponentOrReplace(this.videoMaterial)
        this.state = this.LOADED
    // }
  }
  playVideo() {
    log("PLAY")
    if (this.videoTexture != null && (this.state == this.LOADED || this.state == this.PAUSED)) {
        this.videoTexture.play()
        this.state = this.PLAYING
    }
  }
  pauseVideo() {
    log("PAUSE")
    if (this.videoTexture != null) {
        this.videoTexture.pause()
        this.state = this.PAUSED
    }
  }
  stopVideo() {
    if (this.videoTexture != null) {
        this.videoTexture.reset()
        this.state = this.LOADED
    }
  }
  update(dt: number) {
    if (this.admit == 0) {
        // TODO display instructions on screen
        // TODO if the Payment UI isn't showing, show it
    }
    else { // every 1/3 second, or so:
        // TODO get the mode, catalog, currentlyPlaying and seek position from server
        // TODO if they have changed, change this client's mode, catalog, currentlyPlaying,
        // TODO (only change them if changed, only change seek if it is significantly off)
    }
  }
}


let scene = spawnEntity(61,0,32,  0,0,0, 1,1,1)

/////////////////// Screen /////////////////////

const screen = new Entity();
screen.addComponent(new PlaneShape());
screen.addComponent(new Transform({
    position: new Vector3(8,4,15),
    rotation: Quaternion.Euler(0, 180, 0),
    scale: new Vector3(15,8,1)
}));

engine.addEntity(screen);


let cinemaClient = new CinemaClient(null, null, screen, null) 


// import {BuilderHUD} from 'modules/BuilderHUD'
// let builderHUD = new BuilderHUD()
//builderHUD.attachToEntity(scene)
// // builderHUD.attachToEntity(theater_building)
// // builderHUD.attachToEntity(theater_interior)
// builderHUD.attachToEntity(screen)

