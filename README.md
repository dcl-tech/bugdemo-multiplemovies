Demo of issue in Decentraland SDK


The issue is that if you pick a different movie and start it, the soundtracks from previously-played movies also play, and pausing the new movie doesn't pause the old soundtracks.

Steps to reproduce

Load scene in SDK preview

Select a movie
- Leftmost cube selects previous movie from catalog
- Rightmost cube selects next movie catalog
- There are 3 movies in the catalog

Press the 2nd cube from the left (Play)
- (Play resets any currently-playing movie, loads ths selected movie into the screen, and plays it)

// movie plays

Select a different movie

Press Play (2nd cube)
- // expected: original movie stops, new movie plays
- // actual: original movie visual stops, new movie plays, but so does the sound from the original movie.

(The 3rd button is a pause button.)

Press Pause when a subsequent mnovie is playing
- // expected: vidoe and sound stop
- // actual: sound from previously played movies continues playing
